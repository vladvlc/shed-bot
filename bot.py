from datetime import datetime, timedelta

import telegram
import logging
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, \
    CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

from settings import TOKEN
from shed import shed


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

default_date = datetime(2017, 9, 4)

RETARD_MSG = "Neh"


def _get_week_type(tomorrow=False):
    delta = timedelta(0)

    if tomorrow:
        delta = timedelta(days=1)

    week_number, _ = divmod(datetime.now() + delta - default_date, timedelta(days=7))

    return "odd" if week_number % 2 else "even"


def start(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id, text='Здарова! Жми /help')


def help(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id, text="Send /day for day schedule\n"
                              "Send /week for week type...\n"
                              "Send /today for today schedule...\n"
                              "Send /tomorrow ну ты понял зачем...\n"
                              "Other shit in progress, send your requests)")


def get_week_type(bot, update):
    week_type = _get_week_type()
    text = "Четная" if week_type == "even" else "Нечетная"
    bot.sendMessage(chat_id=update.message.chat_id, text=text)


def _get_schedule(week_day, week_type_arg=False):
    week_type = _get_week_type(week_type_arg)

    today_shed = shed[week_day]
    out_shed = ""

    for pair in today_shed:
        if pair.w_type == "contemp" or pair.w_type == week_type:
            out_shed += "*{:%H:%M}* в *{:>4}*\n{:^}\n".format(
                pair.time,
                pair.place,
                pair.name
            )

    return out_shed


def today(bot, update, args):
    week_day = update.message.date.weekday()

    if not week_day in shed.keys():
        bot.sendPhoto(chat_id=update.message.chat_id,
            photo='https://upload.wikimedia.org/wikipedia/en/6/63/Feels_good_man.jpg')
    else:
        bot.sendMessage(
            chat_id=update.message.chat_id,
            text=_get_schedule(week_day),
            parse_mode=telegram.ParseMode.MARKDOWN
        )


def tomorrow(bot, update, args):
    week_day = update.message.date.weekday() + 1

    if week_day == 7:
        week_day = 0

    if not week_day in shed.keys():
        bot.sendPhoto(chat_id=update.message.chat_id,
            photo='https://upload.wikimedia.org/wikipedia/en/6/63/Feels_good_man.jpg')
    else:
        bot.sendMessage(
            chat_id=update.message.chat_id,
            text=_get_schedule(week_day, week_type_arg=True),
            parse_mode=telegram.ParseMode.MARKDOWN
        )


def day(bot, update):
    keyboard = []
    for d in ((1, 'Вторник'), (2, 'Среда'), (3, 'Четверг'), (5, 'Суббота')):
        choice = str(d[0]) + "|" + d[1]
        keyboard.append([InlineKeyboardButton(d[1], callback_data=choice)])

    reply_markup = InlineKeyboardMarkup(keyboard)

    update.message.reply_text('Выбери день (теперь на русском!):', reply_markup=reply_markup)


def day_button(bot, update):
    query = update.callback_query
    choice = query.data.split('|')
    week_day = int(choice[0])

    if not week_day in shed.keys():
        bot.editMessageText(text="*{}* - day off".format(choice[1]),
                        chat_id=query.message.chat_id,
                        message_id=query.message.message_id,
                        parse_mode=telegram.ParseMode.MARKDOWN)
    else:
        schedule = "Schedule for *{}*\n".format(choice[1]) + \
            _get_schedule(week_day=int(choice[0]))

        bot.editMessageText(text=schedule,
                        chat_id=query.message.chat_id,
                        message_id=query.message.message_id,
                        parse_mode=telegram.ParseMode.MARKDOWN)


if __name__ == '__main__':
    updater = Updater(token=TOKEN)

    dp = updater.dispatcher

    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(CommandHandler('help', help))

    dp.add_handler(CommandHandler('day', day))
    dp.add_handler(CallbackQueryHandler(day_button))

    dp.add_handler(CommandHandler('today', today, pass_args=True))
    dp.add_handler(CommandHandler('tomorrow', tomorrow, pass_args=True))
    dp.add_handler(CommandHandler('week', get_week_type))


    updater.start_polling()

    updater.idle()
