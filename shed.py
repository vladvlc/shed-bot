from collections import namedtuple
from datetime import time, datetime

Pair = namedtuple('Pair', ['time', 'name', 'place', 'w_type'])

shed = {
    1: [
        Pair(time(13, 40), "Распределенная обработка информации", 1111, "odd"),
        Pair(time(15, 20), "Распределенная обработка информации", 1113, "contemp"),
        Pair(time(17, 00), "Теория игр и принятие решений", 1010, "contemp"),
    ],
    2: [
        Pair(time(13, 40), "Социально этические вопросы", 1113, "even"),
        Pair(time(15, 20), "Теория кодирования", 1211, "contemp"),
        Pair(time(17, 00), "Интеллектуальные системы", 1113, "contemp"),
        Pair(time(18, 40), "Компьютерное зрение", 1008, "contemp")
    ],
    3: [
        Pair(time(13, 40), "Интеллектуальные системы", 810, "odd"),
        Pair(time(13, 40), "Теория кодирования информации", 810, "even"),
        Pair(time(15, 20), "ИБКС", 216, "contemp"),
        Pair(time(17, 00), "Компьютерное зрение", 1009, "even"),
    ],
    5: [
        Pair(time(8, 30), "Гибкая разработка ПО", 808, "odd"),
        Pair(time(13, 40), "Моделирование инф. процессов", 809, "even"),
        Pair(time(10, 10), "Гибкая разработка ПО", 907, "even"),
        Pair(time(10, 10), "Моделирование инф. процессов", 907, "odd"),
        Pair(time(11, 50), "ИБКС", 909, "odd"),
        Pair(time(11, 50), "Теория игр", 1008, "even"),
    ]
}
