import datetime

import telegram
from apscheduler.schedulers.blocking import BlockingScheduler

from settings import TOKEN, GROUP, MY
from bot import _get_schedule
from shed import shed

scheduler = BlockingScheduler()

@scheduler.scheduled_job('cron', day_of_week='mon-fri', hour=20, minute=15)
def scheduled_job():
    bot = telegram.Bot(TOKEN)
    week_day = (datetime.datetime.now() + datetime.timedelta(hours=3)).weekday() + 1
    print('ok')

    text = '*Расписание на завтра*\n' + _get_schedule(week_day, week_type_arg=True)
    bot.sendMessage(
        chat_id=GROUP,
        text=text,
        parse_mode=telegram.ParseMode.MARKDOWN
    )

scheduler.start()